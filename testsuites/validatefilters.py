# -*- Mode: Python -*- vi:si:et:sw=4:sts=4:ts=4:syntax=python
#
# Copyright (c) 2015,Thibault Saunier <thibault.saunier@collabora.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA 02110-1301, USA.

"""
The GstValidate default testsuite
"""

import os


TEST_MANAGER  = "validate"

def setup_tests(test_manager, options):
    print("Setting up tests to validate all filter elements")

    valid_scenarios = ["play_15s",
                       "fast_forward",
                       "seek_forward",
                       "seek_backward",
                       "seek_with_stop",
                       "scrub_forward_seeking"]

    # No restriction about scenarios that are potentially used
    test_manager.add_scenarios([scenario.name for scenario in
                                test_manager.scenarios_manager.get_scenario(None)])

    test_manager.add_generators(test_manager.GstValidatePipelineTestsGenerator("misc_pipelines", test_manager,
                                pipelines_descriptions=[("zetest", "videotestsrc ! %(videosink)s",
                                                         {'duration':1,
                                                          'plays-reverse': True})],
                                valid_scenarios=valid_scenarios))

    return True
